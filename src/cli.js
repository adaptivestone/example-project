import Cli from '@adaptivestone/framework/Cli.js';
import folderConfig from './folderConfig.js';

const cli = new Cli(folderConfig);

cli.run();
